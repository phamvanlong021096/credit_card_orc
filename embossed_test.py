import cv2
import numpy as np
from matplotlib import pyplot as plt


def show_image(image):
    cv2.imshow("image", image)
    cv2.waitKey(0)


def embossed_preprocess(image):
    # step 1
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    show_image(gray)
    img_h, img_w = gray.shape

    # step 2
    Ie = cv2.Canny(gray, 100, 200)
    show_image(Ie)

    # step 3
    Ib = cv2.threshold(Ie, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    show_image(Ib)

    # step 4
    S = np.zeros((img_h, img_w), dtype=np.uint8)

    # step 5
    Wout = 12
    Win = Wout // 2
    startXY = Win
    endY = img_h - Win
    endX = img_w - Win



def main():
    path = "D:/data/credit_card/mufj/raw_image/test1.png"
    image = cv2.imread(path)

    embossed_preprocess(image)


if __name__ == '__main__':
    main()
