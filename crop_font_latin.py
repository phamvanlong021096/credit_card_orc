import os
import cv2
import imutils


def show_image(image):
    cv2.imshow('img', image)
    cv2.waitKey(0)


def main():
    path = "D:/data/credit_card/font.png"
    out_fir = "D:/data/credit_card/font"

    image = cv2.imread(path)
    image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_thres = cv2.threshold(image_gray, 10, 255, cv2.THRESH_BINARY_INV)[1]

    cnts = cv2.findContours(image_thres, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    i = 0

    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        roi = image_thres[y:y + h, x:x + w]
        roi = cv2.resize(roi, (60, 90))
        i += 1
        out_path = os.path.join(out_fir, str(i) + '.jpg')
        cv2.imwrite(out_path, roi)
        # cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 1)

    print('Done !')


if __name__ == '__main__':
    main()
