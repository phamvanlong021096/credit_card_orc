import os
import cv2
import imutils
import pytesseract
import numpy as np
import xml.etree.ElementTree as ET

from PIL import Image
from constant import *
from transform import four_point_transform


def show_image(image):
    cv2.imshow('image', image)
    cv2.waitKey(0)


def get_sample(font_dir):
    samples = {}
    for d in os.listdir(font_dir):
        for f in os.listdir(os.path.join(font_dir, d)):
            name = os.path.splitext(f)[0]
            image = cv2.imread(os.path.join(font_dir, d, f))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            image = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY)[1]
            image = cv2.resize(image, (SAMPLE_W, SAMPLE_H))
            if name in samples:
                samples[name].append(image)
            else:
                samples[name] = [image]

    return samples


def read_config(path):
    tree = ET.parse(path)
    root = tree.getroot()
    res = {}

    for object in root.findall('object'):
        name = object.find('name').text
        bbox = object.find('bndbox')
        x1 = int(bbox.find('xmin').text)
        y1 = int(bbox.find('ymin').text)
        x2 = int(bbox.find('xmax').text)
        y2 = int(bbox.find('ymax').text)
        res[name] = (x1, y1, x2, y2)

    return res


def crop_and_align_card(image):
    # add border
    image = cv2.copyMakeBorder(image, top=10, bottom=10, left=10, right=10,
                               borderType=cv2.BORDER_CONSTANT, value=[255, 255, 255])

    # resize and convert to gray
    ratio = image.shape[0] / 500.0
    orig_image = image.copy()
    image = imutils.resize(image, height=500)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    show_image(image)

    image = cv2.medianBlur(image, 5)
    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 101, 2)

    show_image(image)

    # find contour
    cnts = cv2.findContours(image.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    # find 4 corners of card
    max_area = 0
    corners = None
    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.015 * peri, True)
        if 2 * h >= w >= h >= 20 and w * h > max_area and len(approx) == 4:
            max_area = w * h
            corners = approx

    # crop and align card
    if corners is not None:
        image_card = four_point_transform(orig_image, corners.reshape(4, 2) * ratio)
        return image_card

    return None


def detect_number(image, bbox, sample_numbers):
    image = cv2.resize(image, (IMAGE_W, IMAGE_H))
    image = image[bbox[1]: bbox[3], bbox[0]: bbox[2]]
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # lam ro anh
    rect_kernel_tophat = cv2.getStructuringElement(cv2.MORPH_RECT, (15, 15))
    image = cv2.morphologyEx(image, cv2.MORPH_TOPHAT, rect_kernel_tophat)

    image = cv2.threshold(image, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # find contours
    cnts = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    locs = []
    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        if h >= w >= 5:
            locs.append((x, y, w, h))
    locs = sorted(locs, key=lambda x: x[0])

    # detect number
    numbers = ''
    for l in locs:
        roi = image[l[1]: l[1] + l[3], l[0]: l[0] + l[2]]
        roi = cv2.resize(roi, (SAMPLE_W, SAMPLE_H))

        number = None
        max_score = 0
        for num, images in sample_numbers.items():
            for img in images:
                result = cv2.matchTemplate(roi, img, cv2.TM_CCOEFF)
                _, score, _, _ = cv2.minMaxLoc(result)
                if score > max_score:
                    max_score = score
                    number = num

        numbers = numbers + str(number)

    return numbers


def detect_letter_tesseract(image, bbox, lang):
    image = cv2.resize(image, (IMAGE_W, IMAGE_H))
    image = image[bbox[1]: bbox[3], bbox[0]: bbox[2]]
    image = cv2.resize(image, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    dil_kernel = np.ones((1, 1), np.uint8)
    e_kernel = np.ones((1, 1), np.uint8)
    dil = cv2.dilate(gray, dil_kernel, iterations=1)
    erode = cv2.erode(dil, e_kernel, iterations=1)

    # Apply blur to smooth out the edges
    blur = cv2.GaussianBlur(erode, (1, 1), 0)
    thres = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    kernel = np.ones((1, 1), np.uint8)
    closing = cv2.morphologyEx(thres, cv2.MORPH_CLOSE, kernel)

    # write the grayscale image to disk as a temporary file so we can apply OCR to it
    filename = '{}.png'.format(os.getpid())
    cv2.imwrite(filename, closing)

    # load the image as a PIL/Pillow image, apply OCR, and then delete the temporary file
    text = pytesseract.image_to_string(Image.open(filename), lang=lang)
    os.remove(filename)

    return text


def draw_rectangle(image, bbox):
    cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 0, 255), 2)
    return image
