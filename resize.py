import os
import cv2
from constant import *


def main():
    dir = "D:/data/credit_card/mufj/raw_image"
    out_dir = "D:/data/credit_card/mufj/samples/images"

    files = os.listdir(dir)
    for f in files:
        in_path = os.path.join(dir, f)
        out_path = os.path.join(out_dir, f)
        image = cv2.imread(in_path)
        image = cv2.resize(image, (IMAGE_W, IMAGE_H))
        cv2.imwrite(out_path, image)

    print("Done !")


if __name__ == '__main__':
    main()
