import cv2
import xml.etree.ElementTree as ET


def read_labels(path):
    tree = ET.parse(path)
    root = tree.getroot()
    res = {}

    for object in root.findall('object'):
        name = object.find('name').text
        bbox = object.find('bndbox')
        x1 = int(bbox.find('xmin').text)
        y1 = int(bbox.find('ymin').text)
        x2 = int(bbox.find('xmax').text)
        y2 = int(bbox.find('ymax').text)
        res[name] = (x1, y1, x2, y2)

    return res


def main():
    sample_label_path = "D:/data/credit_card/mufj/samples/labels/3.xml"
    image_path = "D:/data/credit_card/mufj/samples/images/3.jpg"

    labels = read_labels(sample_label_path)
    image = cv2.imread(image_path)
    for name, bbox in labels.items():
        cv2.rectangle(image, (bbox[0], bbox[1]), (bbox[2], bbox[3]), (0, 0, 255), 1)

    cv2.imshow("img", image)
    cv2.waitKey(0)


if __name__ == '__main__':
    main()
