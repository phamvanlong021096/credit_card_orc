import cv2
import imutils

from utils import get_sample, read_config, crop_and_align_card, detect_number, detect_letter_tesseract


def show_image(image):
    cv2.imshow("img", image)
    cv2.waitKey(0)


def main():
    image_path = "D:/data/credit_card/mufj/test.jpg"
    sample_config_path = "./config/test.xml"
    sample_numbers = get_sample('./font/number')
    sample_letters = get_sample('./font/letter/japan')

    image = cv2.imread(image_path)
    image = crop_and_align_card(image)
    image = imutils.resize(image, height=500)

    show_image(image)

    if image is not None:
        res = {}
        config = read_config(sample_config_path)
        if 'card_number' in config:
            res['card_number'] = detect_number(image, config['card_number'], sample_numbers)

        if 'money_number' in config:
            res['money_number'] = detect_number(image, config['money_number'], sample_numbers)

        if 'name_jpn' in config:
            res['name'] = detect_number(image, config['name_jpn'], sample_letters)

        if 'name_latin' in config:
            res['name'] = detect_letter_tesseract(image, config['name_latin'], lang='eng')

        print(res)

    else:
        print("Card not found!")


if __name__ == '__main__':
    main()
