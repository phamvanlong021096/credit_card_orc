import os
import cv2
import imutils
import pytesseract

from PIL import Image


def show_image(image):
    cv2.imshow('img', image)
    cv2.waitKey(0)


def main():
    path = "D:/data/credit_card/font2.png"
    out_fir = "D:/data/credit_card/font"

    image = cv2.imread(path)
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    img_thres = cv2.threshold(img_gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    show_image(img_thres)

    # gop cac vung lai
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 32))
    sq_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (9, 9))
    img_tophat_2 = cv2.morphologyEx(img_thres, cv2.MORPH_CLOSE, rect_kernel)
    show_image(img_tophat_2)
    img_tophat_3 = cv2.morphologyEx(img_tophat_2, cv2.MORPH_CLOSE, sq_kernel)

    show_image(img_tophat_3)

    cnts = cv2.findContours(img_tophat_3, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    i = 0

    for c in cnts:
        x, y, w, h = cv2.boundingRect(c)
        roi = img_thres[y:y + h, x:x + w]
        # roi = cv2.resize(roi, (60, 90))
        i += 1
        out_path = os.path.join(out_fir, str(i) + '.jpg')
        cv2.imwrite(out_path, roi)
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 1)

    show_image(image)

    print('Done !')


# def main():
#     path = "D:/data/credit_card/font3.png"
#     out_fir = "D:/data/credit_card/font"
#
#     image = cv2.imread(path)
#     h, w, _ = image.shape
#     # image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#     # image_thres = cv2.threshold(image_gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
#     # image = cv2.cvtColor(image_thres, cv2.COLOR_GRAY2BGR)
#
#     text = pytesseract.image_to_boxes(Image.fromarray(image), lang='jpn')
#
#     print(text)
#
#     boxes = []
#     for row in text.splitlines():
#         r = row.split()
#         boxes.append((r[0], int(r[1]), h - int(r[2]), int(r[3]), h - int(r[4])))
#
#     for b in boxes:
#         cv2.rectangle(image, (b[1], b[2]), (b[3], b[4]), (0, 0, 255), 1)
#
#     show_image(image)


if __name__ == '__main__':
    main()
