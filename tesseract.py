import os
import cv2
import pytesseract
import imutils
import numpy as np

from PIL import Image


def show_image(image):
    cv2.imshow("Image", image)
    cv2.waitKey(0)


image_path = "D:/data/credit_card/mufj/raw_image/6.jpg"
preprocess = "thresh"

# load the example image and convert it to grayscale
image = cv2.imread(image_path)
# # image = imutils.resize(image, height=60)
# image = cv2.resize(image, None, fx=1.5, fy=1.5, interpolation=cv2.INTER_CUBIC)
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# show_image(gray)
#
# # # check to see if we should apply preprocess
# # if preprocess == 'thresh':
# #     gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# # elif preprocess == 'blur':
# #     gray = cv2.medianBlur(gray, 3)
#
# dil_kernel = np.ones((1, 1), np.uint8)
# e_kernel = np.ones((1, 1), np.uint8)
# dil = cv2.dilate(gray, dil_kernel, iterations=1)
# show_image(dil)
# erode = cv2.erode(dil, e_kernel, iterations=1)
# show_image(erode)
#
# # Apply blur to smooth out the edges
# blur = cv2.GaussianBlur(erode, (1, 1), 0)
# show_image(blur)
#
# rect_kernel_tophat = cv2.getStructuringElement(cv2.MORPH_RECT, (15, 15))
#
# # # lam ro anh
# # tophat = cv2.morphologyEx(blur, cv2.MORPH_TOPHAT, rect_kernel_tophat)
# # show_image(tophat)
#
# thres = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
# show_image(thres)
#
# kernel = np.ones((1, 1), np.uint8)
# closing = cv2.morphologyEx(thres, cv2.MORPH_CLOSE, kernel)
# show_image(closing)
#
# # write the grayscale image to disk as a temporary file so we can apply OCR to it
# filename = '{}.png'.format(os.getpid())
# cv2.imwrite(filename, closing)

# load the image as a PIL/Pillow image, apply OCR, and then delete the temporary file
text = pytesseract.image_to_string(Image.fromarray(image), lang='jpn')
# os.remove(filename)
print(text)

# show the output images
