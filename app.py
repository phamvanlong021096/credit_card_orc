import os
import cv2
import json
import numpy as np

from flask import Flask, request, jsonify
from utils import get_sample, read_config, crop_and_align_card, detect_number, detect_letter_tesseract, draw_rectangle

app = Flask(__name__)


@app.route('/extract_card', methods=['POST'])
def detect_api():
    sample_config_path = "./config/2.xml"

    # read image file string data
    filestr = request.files['image'].read()
    # convert string data to numpy array
    npimg = np.fromstring(filestr, np.uint8)
    # convert numpy array to image
    image = cv2.imdecode(npimg, cv2.IMREAD_UNCHANGED)

    # crop card
    image = crop_and_align_card(image)

    if image is not None:
        res = {}
        config = read_config(sample_config_path)
        if 'card_number' in config:
            res['card_number'] = detect_number(image, config['card_number'], sample_numbers)

        if 'money_number' in config:
            res['money_number'] = detect_number(image, config['money_number'], sample_numbers)

        if 'name_jpn' in config:
            res['name'] = detect_letter_tesseract(image, config['name_jpn'], lang='jpn')

        if 'name_latin' in config:
            res['name'] = detect_letter_tesseract(image, config['name_latin'], lang='eng')

        return jsonify({'result': json.dumps(res)})

    else:
        return jsonify({'result': 'Card image not found!'})


@app.route('/extract_card/<int:temp>', methods=['POST'])
def detect_api_with_temp(temp):
    sample_config_path = "./config/" + str(temp) + '.xml'
    if not os.path.exists(sample_config_path):
        return jsonify({'result:' 'Template ' + str(temp) + ' not found!'})

    # read image file string data
    filestr = request.files['image'].read()
    # convert string data to numpy array
    npimg = np.fromstring(filestr, np.uint8)
    # convert numpy array to image
    image = cv2.imdecode(npimg, cv2.IMREAD_UNCHANGED)

    # crop card
    image = crop_and_align_card(image)
    image_draw = image.copy()

    if image is not None:
        res = {}
        config = read_config(sample_config_path)
        if 'card_number' in config:
            res['card_number'] = detect_number(image, config['card_number'], sample_numbers)
            image_draw = draw_rectangle(image_draw, config['card_number'])

        if 'money_number' in config:
            res['money_number'] = detect_number(image, config['money_number'], sample_numbers)
            image_draw = draw_rectangle(image_draw, config['money_number'])

        if 'name_jpn' in config:
            res['name'] = detect_letter_tesseract(image, config['name_jpn'], lang='jpn')
            image_draw = draw_rectangle(image_draw, config['name_jpn'])

        if 'name_latin' in config:
            res['name'] = detect_letter_tesseract(image, config['name_latin'], lang='eng')
            image_draw = draw_rectangle(image_draw, config['name_latin'])

        return jsonify({'result': json.dumps(res)})

    else:
        return jsonify({'result': 'Card image not found!'})


if __name__ == '__main__':
    # read sample
    sample_numbers = get_sample('./font/number')
    app.run(host='0.0.0.0', port=5000)
